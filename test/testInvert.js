const object = require('../object')
const invert = require('../invert')

let invertObject = invert(object)

for (let key in object) {
    let value = object[key]
    if (invertObject[value] != key) {
        console.log('Test case is failed : key vales not inverted')
        return
    }

}
console.log("Test case is Passed : key values are inverted")
console.log(invertObject)

