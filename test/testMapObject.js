const object = require('../object')
const mapObject = require('../mapObject')


const transform = "_someInfo"

function callback(value) {
    return value + transform
}

let transformedObject = mapObject(object, callback)
for (let key in object) {
    if (object[key] === transformedObject[key]) {
        console.log("Test Case is Failed : Didnt transformed")
        return
    }
}
console.log("Test case is Passed : object values transformed")
console.log(transformedObject)