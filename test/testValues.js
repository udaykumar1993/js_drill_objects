const object = require('../object')
const values = require('../values')

let valuesArray = values(object)

if (valuesArray.length > 0) {
    console.log('Test case is passed : The object values are ' + valuesArray)
} else {
    console.log('Test case is Failed : No values found in object')
}