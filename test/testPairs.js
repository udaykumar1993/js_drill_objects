const object = require('../object')
const pairs = require('../pairs')

let pairsArray = pairs(object)
if (pairsArray.length > 0) {
    console.log('Test case is Passed :  key value pairs are ')
    for (let index = 0; index < pairsArray.length; index++) {
        console.log(pairsArray[index][0] + " : " + pairsArray[index][1])
    }
} else {
    console.log('Test case is Failed : given object is empty')
}