const object = require('../object.js')
const keys = require('../keys')

let namesArray = keys(object)

if (namesArray.length > 0) {
    console.log('Test case is Passed : keys are ' + namesArray)
} else {
    console.log('Test case is Failed : Object is empty')
}
