function defaults(object, defaultProps) {
    Object.keys(object).map(key => {
        if (object[key] === undefined) {
            object[key] = defaultProps[key]
        }
    })
    return object
}

module.exports = defaults;


