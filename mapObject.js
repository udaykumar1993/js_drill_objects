function mapObject(object, callback) {
    let transformedObject = {}
    Object.keys(object).map(key => {
        let transformValue = callback(object[key])
        transformedObject[key] = transformValue
    })
    return transformedObject
}

module.exports = mapObject;
