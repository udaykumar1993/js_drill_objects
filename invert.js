function invert(object) {
    let invertObject = {}
    for (let key in object) {
        invertObject[object[key]] = key
    }
    return invertObject
}

module.exports = invert