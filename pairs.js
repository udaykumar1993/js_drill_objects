function pairs(object) {
    let pairsArray = []
    for (let key in object) {
        pairsArray.push([key, object[key]])
    }
    return pairsArray
}

module.exports = pairs
